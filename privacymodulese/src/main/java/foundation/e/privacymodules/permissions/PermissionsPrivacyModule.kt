package foundation.e.privacymodules.permissions

import android.app.AppOpsManager
import android.app.AppOpsManager.OP_NONE
import android.app.AppOpsManager.strOpToOp
import android.content.Context
import android.net.IConnectivityManager
import android.os.ServiceManager
import android.os.UserHandle
import android.util.Log
import foundation.e.privacymodules.permissions.data.AppOpModes
import foundation.e.privacymodules.permissions.data.ApplicationDescription

/**
 * Implements [IPermissionsPrivacyModule] with all privileges of a system app.
 */
class PermissionsPrivacyModule(context: Context): APermissionsPrivacyModule(context) {

    private val appOpsManager: AppOpsManager get()
        = context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager

    /**
     * @see IPermissionsPrivacyModule.toggleDangerousPermission
     * Always return true, permission is set using privileged capacities.
     */
    override fun toggleDangerousPermission(
        appDesc: ApplicationDescription,
        permissionName: String,
        grant: Boolean
    ): Boolean {
        try {
            if (grant) {
                context.packageManager.grantRuntimePermission(
                    appDesc.packageName,
                    permissionName,
                    android.os.Process.myUserHandle()
                )
            } else {
                context.packageManager.revokeRuntimePermission(
                    appDesc.packageName,
                    permissionName,
                    android.os.Process.myUserHandle()
                )
            }
        } catch (e: Exception) {
            Log.e("Permissions-e", "Exception while setting permission", e)
            return false
        }

        return true
    }

    override fun setAppOpMode(
        appDesc: ApplicationDescription,
        permissionName: String,
        mode: AppOpModes
    ): Boolean {
        val op = strOpToOp(permissionName)
        if (op != OP_NONE) {
            appOpsManager.setMode(op, appDesc.uid, appDesc.packageName, mode.modeValue)
        }
        return true
    }

    override fun setVpnPackageAuthorization(packageName: String): Boolean {
        val service: IConnectivityManager = IConnectivityManager.Stub.asInterface(
            ServiceManager.getService(Context.CONNECTIVITY_SERVICE))

        try {
            if (service.prepareVpn(null, packageName, UserHandle.myUserId())) {
                // Authorize this app to initiate VPN connections in the future without user
                // intervention.
                service.setVpnPackageAuthorization(packageName, UserHandle.myUserId(), true)
                return true
            }
        } catch (e: java.lang.Exception) {
            Log.e("Permissions-e", "Exception while setting VpnPackageAuthorization", e)
        } catch (e: NoSuchMethodError) {
            Log.e("Permissions-e", "Bad android sdk version", e)
        }
        return false
    }
}
